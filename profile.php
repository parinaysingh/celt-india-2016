<?php
session_start();
if(empty($_SESSION['userid'])){
  header('location: login.html');
  exit();
}
require_once('config.php');
$userid =$_SESSION["userid"];
$res=$conn->query("SELECT cpay, cdel, csch, camb from registration WHERE id = $userid");
$result = $conn->query("SELECT name FROM profile where userid = $userid");
$row = $res->fetch_array();
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="SemiColonWeb" />
  <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="lcss/normstyle.css" type="text/css" />
  <link rel="stylesheet" href="css/dark.css" type="text/css" />
  <link rel="stylesheet" href="css/font-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/animate.css" type="text/css" />
  <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

  <link rel="stylesheet" href="css/responsive.css" type="text/css" />
  
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <link rel="stylesheet" href="css/materialize.css" type="text/css" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>CELT Ambassador Program</title>

</head>

<body class="stretched side-header">
  <div id="wrapper" class="clearfix">
    <?php include('header.php'); ?>
    <section id="content">
      <div class="content-wrap nopadding">
        <div class="container clearfix">
        <?php if(!$result->num_rows){ ?>
          <div class="fancy-title title-dotted-border title-center">
              <h3>Student's Profile</h3>
          </div>
          <form action="profileget.php" id="delegateForm" name="delegateForm" method="post" enctype="multipart/form-data">
            <div class="container">
              <div class="row">
              <div class="col s12">
                        <div class="row">
                          <div class="input-field col s12">
                             <i class="material-icons prefix">account_circle</i>
                             <input required id="icon_prefix" type="text" class="validate" name="name">
                             <label for="icon_prefix">Your Name</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="input-field col s12">
                             <div class="file-field input-field">
                                <div class="btn">
                                   <span>File</span>
                                   <input type="file" name="file">
                                </div>
                                <div class="file-path-wrapper">
                                   <input required class="file-path validate" type="text" placeholder="Upload your Photo (max: 100kb)">
                                </div>
                             </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="input-field col s12">
                              <select name="gender" id="gender">
                                 <option required value="" disabled selected>Choose your option</option>
                                 <option value="male">Male</option>
                                 <option value="female">Female</option>
                              </select>
                              <label>Gender</label>
                           </div>
                       </div><br>
                       <div class="row">
                          <div class="input-field col s12">
                               <i class="material-icons prefix">library_books</i>
                                <input type="date" class="datepicker" name="bday" required>
                                <label for="library_books">Date of Birth</label>
                            </div>
                        </div> 
                            <div class="row">
                                     <div class="input-field col s12">
                                        <i class="material-icons prefix">business</i>
                                        <input required id="business" type="text" class="validate" name="college">
                                        <label for="business">Your University</label>
                                     </div>
                                  <div class="row">
                                  <div class="input-field col s12">
                                       <i class="material-icons prefix">note_add</i>
                                        <input required id="note_add" type="text" class="validate" name="study">
                                        <label for="note_add">Course Of Study</label>
                                  </div>
                                </div>
                                <div class="row">
                                     <div class="input-field col s12">
                                        <i class="material-icons prefix">business</i>
                                        <input required id="business" type="text" class="validate" name="gpa">
                                        <label for="business">GPA</label>
                                     </div>
                                  </div>
                                     <div class="row">
                                     <div class="input-field col s12">
                                       <i class="material-icons prefix">phone</i>
                                        <input required id="icon_telephone" type="tel" class="validate" name="phone">
                                        <label for="icon_telephone">Mobile Number</label>
                                     </div>
                                   </div>
                                      <div class="row">
                                        <div class="input-field col s12">
                                          <i class="material-icons prefix">location_on</i>
                                          <textarea required id="icon_prefix2" class="materialize-textarea" name="address"></textarea>
                                          <label for="icon_prefix2">Address</label>
                                        </div>
                                      </div>
                                  <div class="row">
                                     <div class="input-field col s12">
                                        <label>Do you have a Passport?</label></br>
                                         <p>
                                         <input type="radio" id="passportYes" name="passpt" value="Yes" />
                                         <label for="passportYes">Yes</label>
                                         </p>
                                        <p>
                                        <input type="radio" id="passportNo" name="passpt" value="No"/>
                                        <label for="passportNo">No</label>
                                        </p>
                                      </div>
                                   </div>
                                <div class="row">
                                     <div class="input-field col s12">
                                        <i class="material-icons prefix">business</i>
                                        <input id="business1" type="text" class="validate" name="passport" placeholder="Passport Number" disabled="disabled" required>
                                     </div>
                                     <div class="input-field col s12">
                                        <i class="material-icons prefix">business</i>
                                        <input id="business2" type="text" class="validate" name="passexpdt" placeholder="Passport Expiry Date" disabled="disabled" required>
                                     </div>
                                  </div>
                                  <div class="row">
                                     <div class="input-field col s12">
                                        <i class="material-icons prefix">business</i>
                                        <input id="business3" type="text" class="validate" name="airport" placeholder="Nearest airport"disabled="disabled" required>
                                     </div>
                                </div>
                                <div class="row">
                                      <div class="input-field col s12">
                                        <label>Would you require Visa Assistance to enable you travel to India?</label></br>
                                         <p>
                                            <br />
                                            <input name="visa" type="radio" id="test1" value="Yes" required/>
                                            <label for="test1">Yes</label>
                                          </p>
                                          <p>
                                            <input name="visa" type="radio" id="test2" value="No" required/>
                                            <label for="test2">No</label>
                                          </p>
                                      </div>
                                    </div>
                                 <div class="row">
                                      <div class="input-field col s12">
                                        <label>Do you need any scholarship?</label></br>
                                         <p>
                                         <br />
                                          <input name="scholarship" type="radio" id="test3" value="Yes" required/>
                                          <label for="test3">Yes</label>
                                        </p>
                                        <p>
                                          <input name="scholarship" type="radio" id="test4" value="No" required />
                                          <label for="test4">No</label>
                                        </p>
                                      </div>
                                    </div><br>
                                    <label><h6>Upload your Resume</h6></label>
                                     <div class="file-field input-field">
                                      <div class="btn">
                                        <span>File</span>
                                        <input type="file" name="file1" required>
                                      </div>
                                      <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Upload your Resume">
                                      </div>
                                    </div><br><br><br>
                                   <div class="row">
                                      <div class="input-field col s12">
                                      <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                                        <i class="material-icons right">send</i>
                                      </button>
                                    </div>
                                  </div>
                             </div>
                        </div>
              </div>
        </form>
        
        <?php }else{?>
        <p class="done"> 
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <i class="icon-gift">
              </i>
              <strong>Congratulations!</strong>
              <?php echo "Profile Completed Successfully";}?>

            </div>
        </p>
        </div>
      </div>

    </section>
  </div>
  
  <div id="gotoTop" class="icon-angle-up"></div>

  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/plugins.js"></script>
  <script type="text/javascript" src="js/jquery.calendario.js"></script>
   <script type="text/javascript" src="js/functions.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
    <script type="text/javascript" src="js/init.js"></script>
    <script type="text/javascript" src="js/delegate.js"></script>

  
</body>
</html>

<script type="text/css">
  ::-webkit-input-placeholder {
  text-decoration:uppercase;
}

:-moz-placeholder {
   text-decoration:uppercase;  
}

::-moz-placeholder {
   text-decoration:uppercase; 
}

:-ms-input-placeholder {  
   text-decoration:uppercase;   
}
</script>