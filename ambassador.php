<?php
session_start();
if(empty($_SESSION['userid'])){
	header('location: login.html');
	exit();
}
require_once('config.php');
$userid =$_SESSION["userid"];
$result=$conn->query("SELECT cpay, cdel, csch, camb from registration WHERE id = $userid");
$row = $result->fetch_array();
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" /> 
	<link rel="stylesheet" href="lcss/normstyle.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />

      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <link rel="stylesheet" href="css/materialize.css" type="text/css" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>CELT Ambassador Program</title>

</head>

<body class="stretched side-header">
	<div id="wrapper" class="clearfix">
		<?php include('header.php'); ?>
		<section id="content">
			<div class="content-wrap nopadding">
				<?php 
					if($row['cdel']){
					if(!$row['camb']){
				?>
				<div class="container clearfix">
					<div class="fancy-title title-dotted-border title-center">
							<h3>Conference Ambassador's Application Form</h3>
					</div>
					<form action="ambassadorget.php" id="delegateForm" name="delegateForm" method="post" enctype="multipart/form-data">
						<div class="postcontent nobottommargin  clearfix">
							<div id="posts" class="post-timeline clearfix">
								<div class="timeline-border"></div>
								<div class="entry clearfix">
									<div class="entry-timeline">
										1
										<div class="timeline-divider"></div>
									</div>
									<div class="entry-content">
										<div class="input-field col s6">
											<textarea required name="reason" id="textarea1" class="materialize-textarea" length="500"></textarea>
											  <label for="textarea1">Please describe why you wish to become a <strong>Campus Ambassador.</strong> 
												(Not more than 500 characters)
											  </label>
										  </div>						
									</div>
								</div>
								<div class="entry clearfix">
									<div class="entry-timeline">
										2
										<div class="timeline-divider"></div>
									</div>
									<div class="entry-content">
										<div class="input-field col s6">
											<textarea required name="marketing" id="textarea2" class="materialize-textarea" length="600"></textarea>
											  <label for="textarea2">What marketing efforts would you employ to reach out to your campus? (Not more than 600 characters) 
											  </label>
										  </div>
																
									</div>
								</div>
							</div>
								<div class="row">
										<button class="col s12 btn waves-effect waves-light" type="submit" name="action">Submit Now<i class="material-icons right">send</i></button>
										
								</div>
						</div>		
					</form>
				</div>	
			<?php }else{
				?><div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<i class="icon-gift">
				</i>
				<strong>Congratulations!</strong>
				<p class="done"> <?php echo "Campus Ambassador Application Completed Successfully";
				}?> </p>
				</div>
			<?php
				}else{
					?><div class="alert alert-warning">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					        <i class="icon-gift"></i>
					        <strong><?php echo "Please Complete application.";}?></strong>
						</div>
			
			</div>
		</section>
	</div>
	<div id="gotoTop" class="icon-angle-up"></div>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/plugins.js"></script>
	<script type="text/javascript" src="js/jquery.calendario.js"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>

</body>
</html>