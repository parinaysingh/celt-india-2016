<?php
require_once('config.php');
if(empty($_POST['email1'])){
	echo "Fields missing";
	exit();

}
$pass=$conn->real_escape_string($_POST['pass1']);
$email=$conn->real_escape_string($_POST['email1']);
	$stmt = $conn->prepare("SELECT password from registration WHERE email = ?");
	$stmt->bind_param('s',$email);
    $stmt->execute();
    $stmt->store_result();
	$stmt->bind_result($token);
    if(($stmt->num_rows)==0){
		echo "Email not registered";
	}else{
		$stmt->fetch();
		echo "Password reset link has been sent to Registered email id.";
		$token='http://celtindia.org/dashboard/resetpassword.php?email='.$email.'&token='.$token.'&pass='.$pass;
		//send confirmation mail with link $token
	}
$conn->close();
?>
