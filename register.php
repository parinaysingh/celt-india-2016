<?php
require_once('config.php');
$email=$conn->real_escape_string($_POST['email1']);
$pass= $conn->real_escape_string($_POST['password1']);

$password= password_hash($pass, PASSWORD_BCRYPT, array('cost' => '10'));
$email = filter_var($email, FILTER_SANITIZE_EMAIL);
if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
	echo "Invalid Email";
}else{
  	$stmt1 = $conn->prepare("SELECT * FROM registration WHERE email= ?");
    $stmt1->bind_param('s',$email);
    $stmt1->execute();
    $stmt1->store_result();
	if(($stmt1->num_rows)==0){
		$stmt = $conn->prepare("INSERT into registration (email, password) values (?, ?)");
		$stmt->bind_param('ss', $email, $password);
		$stmt->execute();
		$stmt->store_result();
		$numRows = $stmt->affected_rows;
		if($numRows != 0){
			echo "Successfully Registered. Please confirm mail";
			$token='http://celtindia.org/dashboard/confirmmail.php?email='.$email.'&token='.$password;
			//send confirmation mail with link $token
		}else{
			echo "Error";
		}
	}else{
			echo "This email is already registered!";
	}
}
$conn->close();
?>
