<?php
session_start();
if(empty($_SESSION['userid'])){
	header('location: login.html');
	exit();
}
require_once('config.php');
$userid = $_SESSION['userid'];
$result=$conn->query("SELECT cpay, cdel, csch, camb from registration WHERE id = $userid");
$application=$conn->query("SELECT conf from application WHERE userid = $userid");
$row = $result->fetch_array();
$approw=$application->fetch_array();
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" /> 
	<link rel="stylesheet" href="lcss/normstyle.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <link rel="stylesheet" href="css/materialize.css" type="text/css" />

      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	
	<title>CELT Payment</title>

</head>

<body class="stretched side-header">

	<div id="wrapper" class="clearfix">
		<?php include('header.php'); ?>
		<section id="content">
			<div class="content-wrap nopadding">
				<?php 
					if($row['cdel']){
						if($approw['conf']){
							if(!$row['cpay']){
				?>
				<div class="container clearfix">
					<div class="fancy-title title-dotted-border title-center">
							<h3>CELT Application : Payment</h3>
					</div>
				<form action="paymentget.php" method="post">
					<div class="postcontent nobottommargin  clearfix">
						<div id="posts" class="post-timeline clearfix">
							<div class="timeline-border"></div>
							<div class="entry clearfix">
									<div class="entry-timeline">
										1
										<div class="timeline-divider"></div>
									</div>
									
									<div class="entry-content">
										<div class="entry-title">
										<h5 style="font-size: 1.3rem; color:black !important; text-decoration:none; ">
											<span class="materialize-textarea">
												Your application has been selected. Kindly proceed for the payment by clicking on the given link.<br ><br><b><a href="https://in.explara.com/e/conclave-of-entrepreneurship-leadership-and-technology-celt-india-2016">https://in.explara.com/e/conclave-of-entrepreneurship-leadership-and-technology-celt-india-2016</b></a>.
											</span>
										</h5>
										</div>				
									</div>
								</div>
							
							<div class="entry clearfix">
								<div class="entry-timeline">
									2
									<div class="timeline-divider"></div>
								</div>
								
								<div class="entry-content">
										<div class="entry-title">
										<h5 style="font-size: 1.3rem; color:black !important; text-decoration:none; ">
											<span class="materialize-textarea">
												After successful payment kindly enter the Ticket Id/No. in the given input box.
											</span>
											<br /><br />
									
									<div class="input-field col s6">
						                <input required name="ticket" id="textarea1" class="materialize-textarea" placeholder="eg. IEB3294"></input>
						              </h5>
										</div>				
									</div>
							</div>
	  
							<div class="row">
				      				<button class="col s12 btn waves-effect waves-light" type="submit" name="action">Submit Now<i class="material-icons right">send</i></button>
				      				
				    		</div>
						</div>
					</div>		
			</div>
		</form>
			<?php }else{
				?>
				<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<i class="icon-gift">
				</i>
				<strong>Congratulations!</strong>
				<p class="done"> <?php echo "Payment Completed Successfully";
				}?> </p>
				</div>
				<?php
				}else{
					?><div class="alert alert-warning">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					        <i class="icon-gift"></i>
					        <strong><?php echo "Application under review. Please try again after some time.";}?></strong>
						</div>
			<?php
				}else{
					?><div class="alert alert-warning">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					        <i class="icon-gift"></i>
					        <strong><?php echo "Please Complete application.";}?></strong>
						</div>
			
		</div>

		</section><!-- #content end -->




	</div>
	<div id="gotoTop" class="icon-angle-up"></div>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/plugins.js"></script>
	<script type="text/javascript" src="js/jquery.calendario.js"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
</body>
</html>