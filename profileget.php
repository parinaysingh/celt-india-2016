<?php
require_once('config.php');
session_start();
if(empty($_POST['name']) || empty($_POST['bday'])){
	$msg =  "Please input all fields";
	exit();
}	
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <link href="lcss/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="lcss/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" type="text/css" href="lcss/icon.css">

  <title>Login Success | CELT</title>
<style>
  .title{
    font-size:1.5em;
  }
</style>
</head>
<body>

<?php

	$userid= $_SESSION["userid"];
	$name= $conn->real_escape_string($_POST['name']);
	$file= $_FILES['file'];
	$gender= $conn->real_escape_string($_POST['gender']);
	$bday= $conn->real_escape_string($_POST['bday']);
	$college= $conn->real_escape_string($_POST['college']);
	$study= $conn->real_escape_string($_POST['study']);
	$gpa= $conn->real_escape_string($_POST['gpa']);
	$address= $conn->real_escape_string($_POST['address']);
	$phone= $conn->real_escape_string($_POST['phone']);
	$passport= $conn->real_escape_string($_POST['passport']);
	$passexpdt= $conn->real_escape_string($_POST['passexpdt']);
	$airport= $conn->real_escape_string($_POST['airport']);
	$visa= $conn->real_escape_string($_POST['visa']);
	$scholarship= $conn->real_escape_string($_POST['scholarship']);
	$resume= $_FILES['file1'];
	
		$file_name = $file['name'];
		$file_size = $file['size'];
		$file_ext = explode('.', $file_name);
		$file_ext = strtolower(end($file_ext));
		$file_tmp = $file['tmp_name'];
		$allowed = array('jpg','png');
		if(in_array($file_ext, $allowed)){
			if($file['error'] === 0){
				$file_destination = 'photos/' . $name . '(' . $userid . ')' . '.' . $file_ext;
				if($file_size < 2097152){
					if(move_uploaded_file($file_tmp, $file_destination)){	
						
					}else{
						?>
						<div class="materialContainer">
							   <div class="box">
								 <div class="title">photo couldn't be uploaded! Please try again</div>
						   </div>
						</div>
						<?php
						exit();
					}
				}else{
					?>
						<div class="materialContainer">
						   <div class="box">
								 <div class="title">Please Upload Photo Less than 2mb</div>
						   </div>
						</div>
					<?php
					exit();
				}
			}else{
				?>
				<div class="materialContainer">
				   <div class="box">
					 <div class="title">File Error</div>
				   </div>
				</div>
				<?php		
				exit();
			}
		}else{
			?>
			<div class="materialContainer">
			   <div class="box">
					 <div class="title">Please Upload only .jpg, .png Files for photo</div>
			   </div>
			</div>
			<?php
			exit();
		}
		
		$file_name = $resume['name'];
		$file_size = $resume['size'];
		$file_ext = explode('.', $file_name);
		$file_ext = strtolower(end($file_ext));
		$file_tmp = $resume['tmp_name'];
		$allowed = array('pdf','jpg','png','docx');
		if(in_array($file_ext, $allowed)){
			if($resume['error'] === 0){
				$file_destination = 'resume/' . $name . '(' . $userid . ')' . '.' . $file_ext;
				if($file_size < 2097152){
					if(move_uploaded_file($file_tmp, $file_destination)){	
		
					}else{
						?>
						<div class="materialContainer">
							   <div class="box">
								 <div class="title">resume couldn't be uploaded! Please try again</div>
						   </div>
						</div>
						<?php
						exit();
					}
				}else{
					?>
					<div class="materialContainer">
						   <div class="box">
							 <div class="title">Please Upload resume file Less than 2mb</div>
					   </div>
					</div>
					<?php
					exit();
				}
			}else{
				?>
				<div class="materialContainer">
					   <div class="box">
						 <div class="title">resume File Error</div>
				   </div>
				</div>
				<?php
				exit();
			}
		}else{
			?>
				<div class="materialContainer">
					   <div class="box">
						 <div class="title">Please Upload only .jpg, .png, .pdf, .docx Files for resume</div>
				   </div>
				</div>
				<?php
			exit();
		}
	$query = "INSERT INTO profile (`userid`, `name`, `gender`, `bday`, `college`, `study`, `gpa`, `address`, `phone`, `passport`, `passexpdt`, `airport`, `visa`, `scholarship`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";	
	
	if($stmt=$conn->prepare($query)){
    		$stmt->bind_param("ssssssssssssss",$userid, $name, $gender, $bday, $college, $study, $gpa, $address, $phone, $passport, $passexpdt, $airport, $visa, $scholarship);
    		$stmt->execute();
    		$stmt->close();
			header('location: profile.php');
	}else{
		var_dump($conn->error);
		?>
		<div class="materialContainer">
		   <div class="box">
				 <div class="title">Please Try Again</div>
		   </div>
		</div>
		<?php
	}
?>
</body>
 </html>