<?php
session_start();
if(empty($_SESSION['userid'])){
	header('location: login.html');
	exit();
}
require_once('config.php');
$userid = $_SESSION['userid'];
$result=$conn->query("SELECT cpay, cdel, csch, camb from registration WHERE id = $userid");
$row = $result->fetch_array();
?>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="lcss/normstyle.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<style>
		.hide{
			display:none;
		}
	</style>
	<title>CELT Dashboard</title>
	
</head>

<body class="stretched side-header">
	<span class="hide" id="app"><?php echo $row['cpay']; ?></span>
	<span class="hide" id="cdel"><?php echo $row['cdel']; ?></span>
	<div id="wrapper" class="clearfix">
		<?php include('header.php'); ?>
		<section id="content">
			<div class="content-wrap nopadding">
				<div class="container clearfix">
					<div class="fancy-title title-dotted-border title-center">
						<h3>Application Status</h3>
					</div>
					<div id="processTabs">
						<ul class="process-steps bottommargin clearfix" id="status">
							<li class="">
								<a href="#" class="i-bordered i-circled divcenter icon-edit"></a>
								<h5>Application Submitted</h5>
							</li>
							<li class="">
								<a href="#" class="i-circled i-bordered icon-calendar divcenter"></a>
								<h5>Application Under Review</h5>
							</li>

							<li class="">
								<a href="#" class="i-bordered i-circled divcenter icon-pencil2"></a>
								<h5>Payment Completed</h5>
							</li>
							<li class="">
								<a href="#" class="i-bordered i-circled divcenter icon-ok"></a>
								<h5>Selected</h5>
							</li>
						</ul>
						<div>
							<div class="fancy-title title-dotted-border title-center">
								<h3></h3>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</section>		
	</div>
	<div id="gotoTop" class="icon-angle-up"></div>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/plugins.js"></script>
	<script type="text/javascript" src="js/jquery.calendario.js"></script>

	<script type="text/javascript"> //for application status
		var app=document.getElementById("app");
		var cdel=document.getElementById("cdel");
		var checklist=document.getElementById("status");
		var items=checklist.querySelectorAll("li");
		if(app.innerHTML==1){
			items[3].className="ui-state-default ui-corner-top ui-tabs-active ui-state-active";
		}else	if(cdel.innerHTML==1){
				items[1].className="ui-state-default ui-corner-top ui-tabs-active ui-state-active";
		}
	</script>
	<script type="text/javascript" src="js/functions.js"></script>

</body>
</html>
