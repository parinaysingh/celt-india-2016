<?php
session_start();
if(empty($_SESSION['userid'])){
	header('location: login.html');
	exit();
}
require_once('config.php');
$userid =$_SESSION["userid"];
$result=$conn->query("SELECT cpay, cdel, csch, camb from registration WHERE id = $userid");
$row = $result->fetch_array();
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" /> 
	<link rel="stylesheet" href="lcss/normstyle.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <link rel="stylesheet" href="css/materialize.css" type="text/css" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<title>CELT Ambassador Program</title>

</head>

<body class="stretched side-header">
	<div id="wrapper" class="clearfix">
		<?php include('header.php'); ?>
		<section id="content">
			<div class="content-wrap nopadding">
				<?php 
					if($row['cdel']){
					if(!$row['csch']){
				?>
				<div class="container clearfix">
					<div class="fancy-title title-dotted-border title-center">
							<h3>CELT Scholarship : Registrations</h3>
					</div>
				<form action="scholarshipget.php" id="delegateForm" name="delegateForm" method="post" enctype="multipart/form-data">
					<div class="postcontent nobottommargin  clearfix">
						<div id="posts" class="post-timeline clearfix">
							<div class="timeline-border"></div>
							<div class="entry clearfix">
								<div class="entry-timeline">
									1
									<div class="timeline-divider"></div>
								</div>
								
								<div class="entry-content">
									<div class="entry-title">
									<h5 style="font-size: 1.3rem;"><span>Given the recent rise of global issues ranging from <strong>terrorism (ISIS to Boko Haram)</strong> to health epidemics like <i>Ebola</i> or <i>Zika Outbreak</i>, how much is the practicality of having the existing parameters of 
                                    <i>poverty control, fighting inequality</i> and <i>tackling climate change</i> as the focussing basis of sustainable growth, whereas bigger problems like <strong>health epidemics control, human rights violation, international trade facilitation law 
                                  </strong>needs enforcement as well?
                                    (Not more than 900 characters).</span></h5>
									</div>
									
									<div class="input-field col s6">
						                <textarea required name="globalissue" id="textarea1" class="materialize-textarea" length="900"></textarea>
						              </div>						
								</div>
							</div>

							<div class="entry clearfix">
								<div class="entry-timeline">
									2
									<div class="timeline-divider"></div>
								</div>
								
								<div class="entry-content">
									<div class="input-field col s6">
						                <textarea required name="technology" id="textarea2" class="materialize-textarea" length="500"></textarea>
                                  <label for="textarea2">How do you think technology driven innovation can be a propellant towards a better resurgent world?
                                    (Not more than 500 characters).
                                  </label>
						              </div>
															
								</div>
							</div>

							
							  
							<div class="row">
				      				<button class="col s12 btn waves-effect waves-light" type="submit" name="action">Submit Now<i class="material-icons right">send</i></button>
				      				
				    		</div>
						</div>
					</div>
			</div>
		</form>
			<?php }else{
				?><div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<i class="icon-gift">
				</i>
				<strong>Congratulations!</strong>
				<p class="done"> <?php echo "Scholarship Application Completed Successfully";
				}?> </p>
				</div>
			<?php
				}else{
					?><div class="alert alert-warning">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					        <i class="icon-gift"></i>
					        <strong><?php echo "Please Complete application.";}?></strong>
						</div>
			
		</div>

		</section>
	</div>
	<div id="gotoTop" class="icon-angle-up"></div>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/plugins.js"></script>
	<script type="text/javascript" src="js/jquery.calendario.js"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
	
</body>
</html>