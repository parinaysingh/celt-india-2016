<?php
require_once('config.php');
session_start();
if(empty($_POST['ticket'])){
	echo "Please input all fields";
	exit();
}
$ticket=$conn->real_escape_string($_POST['ticket']);
$userid = $_SESSION["userid"];
if($stmt = $conn->prepare("SELECT used from tickets WHERE ticketnum = ?")){
	$stmt->bind_param('s',$ticket);
    $stmt->execute();
    $stmt->store_result();
	if($stmt->num_rows() === 0){
		echo "Invalid Ticket Number";
	}else{
		$stmt->bind_result($used);
		$stmt->fetch();
		$stmt->close();
		if($used === 0){
			$stmt=$conn->prepare("UPDATE registration SET cpay = 1 WHERE id = ?");
			$stmt->bind_param("i", $userid );
			$stmt->execute();
			$stmt->close();
			$stmt=$conn->prepare("UPDATE tickets SET used = 1 WHERE ticketnum = ?");
			$stmt->bind_param("s", $ticket);
			$stmt->execute();
			$stmt->close();
			header('location: payment.php');
		}else{
			echo "Ticket Already Used. Please Complete Payment to get new ticket number.";
		}
	}
}else{
	echo "Error! Please Try Again.";
}
	$conn->close();
?>
