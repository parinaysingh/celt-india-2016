# README #

Website, Dashboard for CELT India 2016 

CELT was a Conclave organized during 9th-13th of August 2016 with Harvard Project for Asian and International Relations, Harvard University as the partner 

CELT promotes openness of knowledge, network, contacts and opportunities. It conducted several competitions of challenging formats, workshops, guest lectures. The conclave provided a platform to open a dialogue among the global leaders of academia to mainstream entrepreneurship education by providing training and support to entrepreneurship educators to demonstrate effective global models of entrepreneurial development, the challenges faced and the outcomes.

![](previewImages/signup_signin.JPG)
![](previewImages/signup_signin2.JPG)
![](previewImages/home.JPG)
![](previewImages/mob_signup_signin.JPG)
![](previewImages/mob_signup_signin2.JPG)
![](previewImages/mob_home.JPG)