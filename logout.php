<?php
session_start();
session_unset(); 
session_destroy();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" type="text/css" href="css/icon.css">
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" /> 
  <link rel="stylesheet" href="lcss/normstyle.css" type="text/css" />
  <link rel="stylesheet" href="css/dark.css" type="text/css" />
  <link rel="stylesheet" href="css/font-icons.css" type="text/css" />
  <link rel="stylesheet" href="css/animate.css" type="text/css" />
  <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

  <title>Logout | CELT</title>
<style>
  .title{
    font-size:1.5em;
  }
</style>
</head>

  <body>
  	<div class="materialContainer">
       <div class="box">
       <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <i class="icon-gift"></i>Successfully Logged out. You will be redirected to the Homepage.<a href="#" class="alert-link">Home CELTIndia</a>.
            </div>
   </div>
</div>
 </body>
 </html>
